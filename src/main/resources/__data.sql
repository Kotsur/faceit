insert into cuisine_type (id, name)
values (1, 'Polish');
insert into cuisine_type (id, name)
values (2, 'Mexican');
insert into cuisine_type (id, name)
values (3, 'Italian');
insert into additive (id, name)
values (1, 'Ice');
insert into additive (id, name)
values (2, 'Lemon');
insert into course_type (id, name)
values (1, 'Course');
insert into course_type (id, name)
values (2, 'Dessert');
