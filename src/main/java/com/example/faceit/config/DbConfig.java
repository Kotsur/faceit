package com.example.faceit.config;

import com.example.faceit.model.Additive;
import com.example.faceit.model.CourseType;
import com.example.faceit.model.CuisineType;
import com.example.faceit.repository.AdditiveRepository;
import com.example.faceit.repository.CourseTypeRepository;
import com.example.faceit.repository.CuisineTypeRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@EnableWebMvc
@Configuration
@EnableJpaRepositories(basePackages = "com.example.faceit.repository")
public class DbConfig {
    private final AdditiveRepository additiveRepository;
    private final CuisineTypeRepository cuisineRepository;
    private final CourseTypeRepository courseTypeRepository;

    public DbConfig(AdditiveRepository additiveRepository, CuisineTypeRepository cuisineRepository, CourseTypeRepository courseTypeRepository) {
        this.additiveRepository = additiveRepository;
        this.cuisineRepository = cuisineRepository;
        this.courseTypeRepository = courseTypeRepository;
    }

    @PostConstruct
    public void init() {
        List<Additive> additiveList = List.of(new Additive("Ice"), new Additive("Lemon"));
        additiveRepository.saveAll(additiveList);
        List<CuisineType> cuisineTypeList = List.of(new CuisineType("Polish"), new CuisineType("Mexican"), new CuisineType("Italian"));
        cuisineRepository.saveAll(cuisineTypeList);
        List<CourseType> courseTypeList = List.of(new CourseType("Course"), new CourseType("Dessert"));
        courseTypeRepository.saveAll(courseTypeList);
    }
}
