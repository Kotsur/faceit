package com.example.faceit.mappers;

import com.example.faceit.dtos.*;
import com.example.faceit.model.*;
import org.mapstruct.*;

@org.mapstruct.Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING, uses = {Mapper.class})
public interface Mapper {
    Additive toEntity(AdditiveDto additiveDto);

    AdditiveDto toDto(Additive additive);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Additive partialUpdate(AdditiveDto additiveDto, @MappingTarget Additive additive);

    Drink toEntity(DrinkDto drinkDto);

    DrinkDto toDto(Drink drink);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Drink partialUpdate(DrinkDto drinkDto, @MappingTarget Drink drink);

    Drink toEntity(DrinkDtoPost drinkDtoPost);

    DrinkDtoPost toDto1(Drink drink);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Drink partialUpdate(DrinkDtoPost drinkDtoPost, @MappingTarget Drink drink);

    CourseType toEntity(CourseTypeDto courseTypeDto);

    CourseTypeDto toDto(CourseType courseType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CourseType partialUpdate(CourseTypeDto courseTypeDto, @MappingTarget CourseType courseType);

    CuisineType toEntity(CuisineTypeDto cuisineTypeDto);

    CuisineTypeDto toDto(CuisineType cuisineType);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CuisineType partialUpdate(CuisineTypeDto cuisineTypeDto, @MappingTarget CuisineType cuisineType);

    Course toEntity(CourseDto courseDto);

    CourseDto toDto(Course course);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Course partialUpdate(CourseDto courseDto, @MappingTarget Course course);

    OrderCourse toEntity(OrderCourseDto orderCourseDto);

    OrderCourseDto toDto(OrderCourse orderCourse);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    OrderCourse partialUpdate(OrderCourseDto orderCourseDto, @MappingTarget OrderCourse orderCourse);

    OrderDrink toEntity(OrderDrinkDto orderDrinkDto);

    OrderDrinkDto toDto(OrderDrink orderDrink);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    OrderDrink partialUpdate(OrderDrinkDto orderDrinkDto, @MappingTarget OrderDrink orderDrink);

    Order toEntity(OrderDto orderDto);

    OrderDto toDto(Order order);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Order partialUpdate(OrderDto orderDto, @MappingTarget Order order);
}