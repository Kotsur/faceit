package com.example.faceit.exception;

public class CourseTypeNotFoundException extends RuntimeException {
    public CourseTypeNotFoundException(Long id) {
        super("Course type with id " + id + " not found");
    }
}
