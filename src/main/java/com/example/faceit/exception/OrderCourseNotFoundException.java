package com.example.faceit.exception;

public class OrderCourseNotFoundException extends RuntimeException {
    public OrderCourseNotFoundException(Long id) {
        super("Order course with id " + id + " not found");
    }
}
