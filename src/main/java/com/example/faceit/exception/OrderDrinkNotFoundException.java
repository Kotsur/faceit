package com.example.faceit.exception;

public class OrderDrinkNotFoundException extends RuntimeException {
    public OrderDrinkNotFoundException(Long id) {
        super("Order drink with id " + id + " not found");
    }
}
