package com.example.faceit.exception;

public class AdditiveNotFoundException extends RuntimeException{
    public AdditiveNotFoundException(Long id){
        super("Additive with id " + id + " not found");
    }
}
