package com.example.faceit.exception;

public class CuisineTypeNotFoundException extends RuntimeException {
    public CuisineTypeNotFoundException(Long id) {
        super("Cuisine type with id " + id + " not found");
    }
}
