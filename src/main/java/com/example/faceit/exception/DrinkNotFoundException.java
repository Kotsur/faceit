package com.example.faceit.exception;

public class DrinkNotFoundException  extends RuntimeException {
    public DrinkNotFoundException(Long id) {
        super("Drink with id " + id + " not found");
    }
}
