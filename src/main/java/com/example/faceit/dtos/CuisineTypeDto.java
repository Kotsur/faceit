package com.example.faceit.dtos;

import com.example.faceit.model.CuisineType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link CuisineType}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class CuisineTypeDto implements Serializable {
    Long id;
    @NotEmpty
    String name;
}