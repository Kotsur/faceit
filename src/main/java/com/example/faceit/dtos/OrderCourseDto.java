package com.example.faceit.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.example.faceit.model.OrderCourse}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderCourseDto implements Serializable {
    Long id;
    CourseDto course;
}