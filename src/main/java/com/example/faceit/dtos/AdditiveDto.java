package com.example.faceit.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link com.example.faceit.model.Additive}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdditiveDto implements Serializable {
    Long id;
    @NotEmpty
    String name;
}