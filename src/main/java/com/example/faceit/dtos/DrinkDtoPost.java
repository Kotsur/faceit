package com.example.faceit.dtos;

import com.example.faceit.model.Drink;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link Drink}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class DrinkDtoPost implements Serializable {
    @NotEmpty
    String name;
    @PositiveOrZero
    Double price;
}