package com.example.faceit.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Value;

import java.io.Serializable;
import java.util.List;

/**
 * DTO for {@link com.example.faceit.model.Drink}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class DrinkDto implements Serializable {
    Long id;
    @NotEmpty
    String name;
    @PositiveOrZero
    Double price;
    List<AdditiveDto> additives;
}