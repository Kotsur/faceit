package com.example.faceit.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

import java.io.Serializable;
import java.util.List;

/**
 * DTO for {@link com.example.faceit.model.OrderDrink}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDrinkDto implements Serializable {
    Long id;
    DrinkDto drink;
    List<AdditiveDto> additives;
}