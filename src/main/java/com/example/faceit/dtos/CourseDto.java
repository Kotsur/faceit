package com.example.faceit.dtos;

import com.example.faceit.model.Course;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Value;

import java.io.Serializable;

/**
 * DTO for {@link Course}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseDto implements Serializable {
    Long id;
    @NotEmpty
    String name;
    @PositiveOrZero
    Double price;
    CourseTypeDto courseTypeDto;
    CuisineTypeDto cuisineTypeDto;
}