package com.example.faceit.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Value;

import java.io.Serializable;
import java.util.List;

/**
 * DTO for {@link com.example.faceit.model.Order}
 */
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDto implements Serializable {
    Long id;
    List<OrderCourseDto> orderCourses;
    List<OrderDrinkDto> orderDrinks;
}