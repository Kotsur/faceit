package com.example.faceit.service;

import com.example.faceit.dtos.OrderDrinkDto;
import com.example.faceit.dtos.OrderDto;
import com.example.faceit.exception.*;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.*;
import com.example.faceit.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final Mapper mapper;
    private final CourseRepository courseRepository;
    private final DrinkRepository drinkRepository;
    private final AdditiveRepository additiveRepository;
    private final OrderCourseRepository orderCourseRepository;
    private final OrderDrinkRepository orderDrinkRepository;

    public OrderService(OrderRepository orderRepository, Mapper mapper, CourseRepository courseRepository, DrinkRepository drinkRepository, AdditiveRepository additiveRepository, OrderCourseRepository orderCourseRepository, OrderDrinkRepository orderDrinkRepository) {
        this.orderRepository = orderRepository;
        this.mapper = mapper;
        this.courseRepository = courseRepository;
        this.drinkRepository = drinkRepository;
        this.additiveRepository = additiveRepository;
        this.orderCourseRepository = orderCourseRepository;
        this.orderDrinkRepository = orderDrinkRepository;
    }

    public List<OrderDto> getAllOrders() {
        return orderRepository.findAll().stream().map(mapper::toDto).toList();
    }

    public OrderDto findOrderById(Long id) {
        return mapper.toDto(orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id)));
    }

    public OrderDto createOrder(OrderDto orderDto) {
        return mapper.toDto(orderRepository.save(mapper.toEntity(orderDto)));
    }


    public ResponseEntity<?> deleteOrder(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
        orderRepository.delete(order);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public OrderDto addCourse(Long idOrder, Long idCourse) {
        Order order = orderRepository.findById(idOrder).orElseThrow(() -> new OrderNotFoundException(idOrder));
        Course course = courseRepository.findById(idCourse).orElseThrow(() -> new CourseNotFoundException(idCourse));
        OrderCourse orderCourse = new OrderCourse(course);
        order.getOrderCourses().add(orderCourse);
        return mapper.toDto(orderRepository.save(order));
    }

    public OrderDto removeCourse(Long idOrder, Long idCourse) {
        Order order = orderRepository.findById(idOrder).orElseThrow(() -> new OrderNotFoundException(idOrder));
        OrderCourse orderCourse = orderCourseRepository.findByOrderIdAndCourseId(idOrder, idCourse).orElseThrow(() -> new OrderCourseNotFoundException(idCourse));
        order.getOrderCourses().remove(orderCourse);
        return mapper.toDto(orderRepository.save(order));
    }

    public OrderDto addDrink(Long idOrder, Long idDrink) {
        Order order = orderRepository.findById(idOrder).orElseThrow(() -> new OrderNotFoundException(idOrder));
        Drink drink = drinkRepository.findById(idDrink).orElseThrow(() -> new DrinkNotFoundException(idDrink));
        OrderDrink orderDrink = new OrderDrink(drink);
        order.getOrderDrinks().add(orderDrink);
        return mapper.toDto(orderRepository.save(order));
    }

    public OrderDto removeDrink(Long idOrder, Long idOrderDrink) {
        Order order = orderRepository.findById(idOrder).orElseThrow(() -> new OrderNotFoundException(idOrder));
        OrderDrink orderDrink = orderDrinkRepository.findById(idOrderDrink).orElseThrow(() -> new OrderDrinkNotFoundException(idOrderDrink));
        order.getOrderDrinks().remove(orderDrink);
        return mapper.toDto(orderRepository.save(order));
    }

    public OrderDrinkDto addAdditive(Long idOrderDrink, Long idAdditive) {
        OrderDrink orderDrink = orderDrinkRepository.findById(idOrderDrink).orElseThrow(() -> new OrderDrinkNotFoundException(idOrderDrink));
        Additive additive = additiveRepository.findById(idAdditive).orElseThrow(() -> new AdditiveNotFoundException(idAdditive));
        orderDrink.getAdditives().add(additive);
        return mapper.toDto(orderDrinkRepository.save(orderDrink));
    }
    public OrderDrinkDto removeAdditive(Long idOrderDrink, Long idAdditive) {
        OrderDrink orderDrink = orderDrinkRepository.findById(idOrderDrink).orElseThrow(() -> new OrderDrinkNotFoundException(idOrderDrink));
        Additive additive = additiveRepository.findById(idAdditive).orElseThrow(() -> new AdditiveNotFoundException(idAdditive));
        orderDrink.getAdditives().remove(additive);
        return mapper.toDto(orderDrinkRepository.save(orderDrink));
    }
}
