package com.example.faceit.service;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.dtos.DrinkDto;
import com.example.faceit.dtos.DrinkDtoPost;
import com.example.faceit.exception.DrinkNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Additive;
import com.example.faceit.model.Drink;
import com.example.faceit.repository.DrinkRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DrinkService {

    private final DrinkRepository drinkRepository;
    private final Mapper mapper;
    private final AdditiveService additiveService;

    public DrinkService(DrinkRepository drinkRepository, Mapper mapper, AdditiveService additiveService) {
        this.drinkRepository = drinkRepository;
        this.mapper = mapper;
        this.additiveService = additiveService;
    }

    public List<DrinkDto> getDrinks() {
        return drinkRepository.findAll().stream().map(mapper::toDto).toList();
    }

    public DrinkDto creteDrink(DrinkDtoPost drinkDto) {
        Drink drink = mapper.toEntity(drinkDto);
        return mapper.toDto(drinkRepository.save(drink));
    }

    public DrinkDto putDrink(DrinkDtoPost drinkDto, Long id) {
        drinkRepository.findById(id).orElseThrow(() -> new DrinkNotFoundException(id));
        Drink drink = mapper.toEntity(drinkDto);
        drink.setId(id);
        return mapper.toDto(drinkRepository.save(drink));
    }

    public DrinkDto patchDrink(DrinkDtoPost drinkDto, Long id) {
        Drink drink = drinkRepository.findById(id).orElseThrow(() -> new DrinkNotFoundException(id));
        if (drinkDto.getName() != null) {
            drink.setName(drinkDto.getName());
        }
        if (drinkDto.getPrice() != null) {
            drink.setPrice(drinkDto.getPrice());
        }
        return mapper.toDto(drinkRepository.save(drink));
    }

    public DrinkDto addAdditive(Long drinkId, Long additiveId) {
        Drink drink = drinkRepository.findById(drinkId).orElseThrow(() -> new DrinkNotFoundException(drinkId));
        Additive additive = mapper.toEntity(additiveService.findById(additiveId));
        Set<Additive> additiveSet = new HashSet<>(drink.getAdditives());
        additiveSet.add(additive);
        List<Additive> additives = new ArrayList<>(additiveSet);
        drink.setAdditives(additives);
        return mapper.toDto(drinkRepository.save(drink));
    }

    public DrinkDto removeAdditive(Long drinkId, Long additiveId) {
        Drink drink = drinkRepository.findById(drinkId).orElseThrow(() -> new DrinkNotFoundException(drinkId));
        Additive additive = mapper.toEntity(additiveService.findById(additiveId));
        drink.getAdditives().remove(additive);
        return mapper.toDto(drinkRepository.save(drink));
    }
    public ResponseEntity<?> delete(Long id) {
        Drink drink = drinkRepository.findById(id).orElseThrow(() -> new DrinkNotFoundException(id));
        drinkRepository.delete(drink);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
