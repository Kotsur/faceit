package com.example.faceit.service;

import com.example.faceit.dtos.CourseDto;
import com.example.faceit.exception.CourseNotFoundException;
import com.example.faceit.exception.CourseTypeNotFoundException;
import com.example.faceit.exception.CuisineTypeNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Course;
import com.example.faceit.model.CourseType;
import com.example.faceit.model.CuisineType;
import com.example.faceit.repository.CourseRepository;
import com.example.faceit.repository.CourseTypeRepository;
import com.example.faceit.repository.CuisineTypeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    private final CourseRepository courseRepository;
    private final CourseTypeRepository courseTypeRepository;
    private final CuisineTypeRepository cuisineRepository;
    private final Mapper mapper;

    public CourseService(CourseRepository courseRepository, CourseTypeRepository courseTypeRepository, CuisineTypeRepository cuisineRepository, Mapper mapper) {
        this.courseRepository = courseRepository;
        this.courseTypeRepository = courseTypeRepository;
        this.cuisineRepository = cuisineRepository;
        this.mapper = mapper;
    }

    public List<CourseDto> findAll() {
        return courseRepository.findAll().stream().map(mapper::toDto).toList();
    }

    public CourseDto findById(Long courseTypeId) {
        return mapper.toDto(courseRepository.findById(courseTypeId).orElseThrow(() -> new CourseNotFoundException(courseTypeId)));
    }

    public CourseDto createCourse(CourseDto courseDto) {
        return mapper.toDto(courseRepository.save(mapper.toEntity(courseDto)));
    }

    public CourseDto putCourse(CourseDto courseDto, Long id) {
        courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));
        Course course = mapper.toEntity(courseDto);
        course.setId(id);
        return mapper.toDto(courseRepository.save(course));
    }

    public CourseDto patchCourse(CourseDto courseDto, Long id) {
        Course course = courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));
        if (courseDto.getName() != null) {
            course.setName(courseDto.getName());
        }
        if (courseDto.getPrice() != null) {
            course.setPrice(courseDto.getPrice());
        }
        return mapper.toDto(courseRepository.save(course));
    }

    public ResponseEntity<?> deleteCourse(Long id) {
        Course course = courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));
        courseRepository.delete(course);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public CourseDto setType(Long idCourse, Long idType) {
        Course course = courseRepository.findById(idCourse).orElseThrow(() -> new CourseNotFoundException(idCourse));
        CourseType courseType = courseTypeRepository.findById(idType).orElseThrow(() -> new CourseTypeNotFoundException(idType));
        course.setCourseType(courseType);
        return mapper.toDto(courseRepository.save(course));
    }

    public CourseDto removeType(Long idCourse, Long idType) {
        Course course = courseRepository.findById(idCourse).orElseThrow(() -> new CourseNotFoundException(idCourse));
        courseTypeRepository.findById(idType).orElseThrow(() -> new CourseTypeNotFoundException(idType));
        course.setCourseType(null);
        courseRepository.save(course);
        return mapper.toDto(courseRepository.save(course));
    }

    public CourseDto setCuisine(Long idCourse, Long idType) {
        Course course = courseRepository.findById(idCourse).orElseThrow(() -> new CourseNotFoundException(idCourse));
        CuisineType cuisineType = cuisineRepository.findById(idType).orElseThrow(() -> new CuisineTypeNotFoundException(idType));
        course.setCuisineType(cuisineType);
        return mapper.toDto(courseRepository.save(course));
    }

    public CourseDto removeCuisine(Long idCourse, Long idType) {
        Course course = courseRepository.findById(idCourse).orElseThrow(() -> new CourseNotFoundException(idCourse));
        cuisineRepository.findById(idType).orElseThrow(() -> new CuisineTypeNotFoundException(idType));
        course.setCuisineType(null);
        return mapper.toDto(courseRepository.save(course));
    }
}
