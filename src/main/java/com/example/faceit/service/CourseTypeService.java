package com.example.faceit.service;

import com.example.faceit.exception.CourseTypeNotFoundException;
import com.example.faceit.model.CourseType;
import com.example.faceit.dtos.CourseTypeDto;
import com.example.faceit.repository.CourseTypeRepository;
import com.example.faceit.mappers.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseTypeService {
    private final CourseTypeRepository courseTypeRepository;
    private final Mapper mapper;

    public CourseTypeService(CourseTypeRepository courseTypeRepository, Mapper mapper) {
        this.courseTypeRepository = courseTypeRepository;
        this.mapper = mapper;
    }

    public List<CourseTypeDto> findAll() {
        return courseTypeRepository.findAll().stream().map(mapper::toDto).toList();
    }

    public CourseTypeDto findById(Long courseTypeId) {
        return mapper.toDto(courseTypeRepository.findById(courseTypeId).orElseThrow(() -> new CourseTypeNotFoundException(courseTypeId)));
    }

    public CourseTypeDto createCourseType(CourseTypeDto courseTypeDto) {
        return mapper.toDto(courseTypeRepository.save(mapper.toEntity(courseTypeDto)));
    }

    public CourseTypeDto putCourseType(CourseTypeDto courseTypeDto, Long id) {
        courseTypeRepository.findById(id).orElseThrow(() -> new CourseTypeNotFoundException(id));
        CourseType courseType = mapper.toEntity(courseTypeDto);
        courseType.setId(id);
        return mapper.toDto(courseTypeRepository.save(courseType));
    }

    public ResponseEntity<?> deleteCourseType(Long id) {
        CourseType courseType = courseTypeRepository.findById(id).orElseThrow(() -> new CourseTypeNotFoundException(id));
        courseTypeRepository.delete(courseType);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
