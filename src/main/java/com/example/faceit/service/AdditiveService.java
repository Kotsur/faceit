package com.example.faceit.service;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.exception.AdditiveNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Additive;
import com.example.faceit.repository.AdditiveRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditiveService {
    private final Mapper mapper;
    private final AdditiveRepository additiveRepository;

    public AdditiveService(Mapper mapper, AdditiveRepository additiveRepository) {
        this.mapper = mapper;
        this.additiveRepository = additiveRepository;
    }

    public List<AdditiveDto> findAll(){
        return additiveRepository.findAll().stream().map(mapper::toDto).toList();
    }
    public AdditiveDto findById(Long id) {
        Additive additive = additiveRepository.findById(id).orElseThrow(()->new AdditiveNotFoundException(id));
        return mapper.toDto(additive);
    }
    public AdditiveDto creteAdditive(AdditiveDto additiveDto) {
        Additive additive = mapper.toEntity(additiveDto);
        Additive savedAdditive = additiveRepository.save(additive);
        return mapper.toDto(savedAdditive);
    }
    public AdditiveDto updateAdditive(AdditiveDto additiveDto, Long id) {
        additiveRepository.findById(id).orElseThrow(()->new AdditiveNotFoundException(id));
        Additive additive = mapper.toEntity(additiveDto);
        additive.setId(id);
        Additive savedAdditive = additiveRepository.save(additive);
        return mapper.toDto(savedAdditive);
    }

    public ResponseEntity<?> delete(Long id) {
        Additive additive = additiveRepository.findById(id).orElseThrow(()->new AdditiveNotFoundException(id));
        additiveRepository.delete(additive);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
