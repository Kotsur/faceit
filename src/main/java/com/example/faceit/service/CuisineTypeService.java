package com.example.faceit.service;

import com.example.faceit.exception.CuisineTypeNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.dtos.CuisineTypeDto;
import com.example.faceit.model.CuisineType;
import com.example.faceit.repository.CuisineTypeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CuisineTypeService {
    private final CuisineTypeRepository cuisineTypeRepository;
    private final Mapper mapper;

    public CuisineTypeService(CuisineTypeRepository cuisineRepository, Mapper mapper) {
        this.cuisineTypeRepository = cuisineRepository;
        this.mapper = mapper;
    }

    public List<CuisineTypeDto> findAll() {

        return cuisineTypeRepository.findAll().stream().map(mapper::toDto).toList();
    }

    public CuisineTypeDto findById(Long courseTypeId) {
        return mapper.toDto(cuisineTypeRepository.findById(courseTypeId).orElseThrow(() -> new CuisineTypeNotFoundException(courseTypeId)));
    }

    public CuisineTypeDto createCuisineType(CuisineTypeDto cuisineTypeDto) {
        return mapper.toDto(cuisineTypeRepository.save(mapper.toEntity(cuisineTypeDto)));
    }

    public CuisineTypeDto putCuisineType(CuisineTypeDto cuisineTypeDto, Long id) {
        cuisineTypeRepository.findById(id).orElseThrow(() -> new CuisineTypeNotFoundException(id));
        CuisineType cuisineType = mapper.toEntity(cuisineTypeDto);
        cuisineType.setId(id);
        return mapper.toDto(cuisineTypeRepository.save(cuisineType));
    }

    public ResponseEntity<?> deleteCuisineType(Long id) {
        CuisineType cuisineType = cuisineTypeRepository.findById(id).orElseThrow(() -> new CuisineTypeNotFoundException(id));
        cuisineTypeRepository.delete(cuisineType);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
