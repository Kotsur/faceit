package com.example.faceit.controller;

import com.example.faceit.dtos.DrinkDtoPost;
import com.example.faceit.service.DrinkService;
import com.example.faceit.validator.ResponseErrorValidation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/drinks")
public class DrinkController {
    private final DrinkService drinkService;
    private final ResponseErrorValidation responseErrorValidation;

    public DrinkController(DrinkService drinkService, ResponseErrorValidation responseErrorValidation) {
        this.drinkService = drinkService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @GetMapping
    public ResponseEntity<?> drinks() {
        return ResponseEntity.ok(drinkService.getDrinks());
    }
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created drink"),
            @ApiResponse(responseCode = "400", description = "Bad request")
    })
    @PostMapping
    public ResponseEntity<?> addDrink(@Valid @RequestBody DrinkDtoPost drinkDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(drinkService.creteDrink(drinkDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateAllDrink(@Valid @RequestBody DrinkDtoPost drinkDtoPost, BindingResult bindingResult, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(drinkService.putDrink(drinkDtoPost, id));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updateField(@Valid @RequestBody DrinkDtoPost drinkDtoPost, BindingResult bindingResult, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(drinkService.patchDrink(drinkDtoPost, id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteDrink(@PathVariable Long id) {
        return drinkService.delete(id);
    }

    @PatchMapping("/{drinkId}/add-additive/{additiveId}")
    public ResponseEntity<?> addAdditive(@PathVariable Long additiveId, @PathVariable Long drinkId) {
        return ResponseEntity.ok(drinkService.addAdditive(drinkId, additiveId));
    }

    @PatchMapping("/{drinkId}/remove-additive/{additiveId}")
    public ResponseEntity<?> removeAdditive(@PathVariable Long additiveId, @PathVariable Long drinkId) {
        return ResponseEntity.ok(drinkService.removeAdditive(drinkId, additiveId));
    }
}
