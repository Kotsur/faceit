package com.example.faceit.controller;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.repository.AdditiveRepository;
import com.example.faceit.service.AdditiveService;
import com.example.faceit.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/additives")
public class AdditiveController {

    private final AdditiveRepository additiveRepository;
    private final Mapper mapper;
    private final ResponseErrorValidation responseErrorValidation;
    private final AdditiveService additiveService;

    public AdditiveController(AdditiveRepository additiveRepository, Mapper mapper, ResponseErrorValidation responseErrorValidation, AdditiveService additiveService) {
        this.additiveRepository = additiveRepository;
        this.mapper = mapper;
        this.responseErrorValidation = responseErrorValidation;
        this.additiveService = additiveService;
    }

    @GetMapping
    public ResponseEntity<?> getListOfAdditives() {
        return ResponseEntity.ok(additiveService.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getOneAdditive(@PathVariable Long id) {
        return ResponseEntity.ok(additiveService.findById(id));
    }

    @PostMapping
    public ResponseEntity<?> createAdditive(@Valid @RequestBody AdditiveDto additiveDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(additiveService.creteAdditive(additiveDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> allUpdateAdditive(@Valid @RequestBody AdditiveDto additiveDto, BindingResult bindingResult, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(additiveService.updateAdditive(additiveDto, id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAdditive(@PathVariable Long id) {
        return additiveService.delete(id);
    }

}
