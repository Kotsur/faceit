package com.example.faceit.controller;

import com.example.faceit.dtos.OrderDto;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.service.OrderService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {
    private final OrderService orderService;
    private final Mapper mapper;

    public OrderController(OrderService orderService, Mapper mapper) {
        this.orderService = orderService;
        this.mapper = mapper;
    }
    @GetMapping
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return ResponseEntity.ok(orderService.getAllOrders());
    }
    @GetMapping("/{id}")
    public ResponseEntity<OrderDto> getOrderById(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.findOrderById(id));
    }
    @PostMapping
    public ResponseEntity<OrderDto> createOrder(@Valid @RequestBody OrderDto orderDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.badRequest().body(orderDto);
        }
        return ResponseEntity.ok(orderService.createOrder(orderDto));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable Long id) {
        orderService.deleteOrder(id);
        return ResponseEntity.noContent().build();
    }
    @PatchMapping("/{idOrder}/add-course/{idCourse}")
    public ResponseEntity<?> addCourse(@PathVariable Long idOrder, @PathVariable Long idCourse) {
        return ResponseEntity.ok().body(orderService.addCourse(idOrder, idCourse));
    }
    @PatchMapping("/{idOrder}/remove-course/{idCourse}")
    public ResponseEntity<?> removeCourse(@PathVariable Long idOrder, @PathVariable Long idCourse) {
        return ResponseEntity.ok().body(orderService.removeCourse(idOrder, idCourse));
    }
    @PatchMapping("/{idOrder}/add-drink/{idCourse}")
    public ResponseEntity<?> addDrink(@PathVariable Long idOrder, @PathVariable Long idCourse) {
        return ResponseEntity.ok().body(orderService.addDrink(idOrder, idCourse));
    }
    @PatchMapping("/{idOrder}/remove-drink/{idCourse}")
    public ResponseEntity<?> removeDrink(@PathVariable Long idOrder, @PathVariable Long idCourse) {
        return ResponseEntity.ok().body(orderService.removeDrink(idOrder, idCourse));
    }
    @PatchMapping("/{idOrderDrink}/add-additive/{idAdditive}")
    public ResponseEntity<?> addAdditive(@PathVariable Long idOrderDrink, @PathVariable Long idAdditive) {
        return ResponseEntity.ok().body(orderService.addAdditive(idOrderDrink, idAdditive));
    }
    @PatchMapping("/{idOrderDrink}/remove-additive/{idAdditive}")
    public ResponseEntity<?> removeAdditive(@PathVariable Long idOrderDrink, @PathVariable Long idAdditive) {
        return ResponseEntity.ok().body(orderService.removeAdditive(idOrderDrink, idAdditive));
    }
}
