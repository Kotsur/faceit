package com.example.faceit.controller;

import com.example.faceit.dtos.CuisineTypeDto;
import com.example.faceit.service.CuisineTypeService;
import com.example.faceit.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/course/cuisine")
public class CuisineTypeController {
    private final CuisineTypeService cuisineTypeService;
    private final ResponseErrorValidation responseErrorValidation;

    public CuisineTypeController(CuisineTypeService cuisineTypeService, ResponseErrorValidation responseErrorValidation) {
        this.cuisineTypeService = cuisineTypeService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @GetMapping
    public ResponseEntity<?> getAllCuisineTypes() {
        return ResponseEntity.ok(cuisineTypeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCuisineTypeById(@PathVariable Long id) {
        return ResponseEntity.ok(cuisineTypeService.findById(id));
    }

    @PostMapping
    public ResponseEntity<?> createCuisineType(@Valid @RequestBody CuisineTypeDto cuisineTypeDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(cuisineTypeService.createCuisineType(cuisineTypeDto));
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> allowUpdateCuisineType(@PathVariable Long id, @Valid @RequestBody CuisineTypeDto cuisineTypeDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(cuisineTypeService.putCuisineType(cuisineTypeDto,id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourseType(@PathVariable Long id) {
        return cuisineTypeService.deleteCuisineType(id);
    }
}
