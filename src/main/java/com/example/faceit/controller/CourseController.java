package com.example.faceit.controller;

import com.example.faceit.dtos.CourseDto;
import com.example.faceit.service.CourseService;
import com.example.faceit.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/courses")
public class CourseController {
    private final CourseService courseService;
    private final ResponseErrorValidation responseErrorValidation;

    public CourseController(CourseService courseService, ResponseErrorValidation responseErrorValidation) {
        this.courseService = courseService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @GetMapping
    public ResponseEntity<?> getAllCourses() {
        return ResponseEntity.ok(courseService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCourseById(@PathVariable Long id) {
        return ResponseEntity.ok(courseService.findById(id));
    }

    @PostMapping
    public ResponseEntity<?> createCourse(@Valid @RequestBody CourseDto course, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(courseService.createCourse(course));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updateFieldCourse(@Valid @RequestBody CourseDto course, BindingResult bindingResult, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(courseService.patchCourse(course, id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCourse(@Valid @RequestBody CourseDto course, BindingResult bindingResult, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(courseService.putCourse(course, id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable Long id) {
        return courseService.deleteCourse(id);
    }
    @PatchMapping("{idCourse}/set-type/{idType}")
    public ResponseEntity<?> setCourseType(@PathVariable Long idCourse, @PathVariable Long idType) {
        return ResponseEntity.ok(courseService.setType(idCourse, idType));
    }

    @PatchMapping("{idCourse}/remove-type/{idType}")
    public ResponseEntity<?> removeCourseType(@PathVariable Long idCourse, @PathVariable Long idType) {
        return ResponseEntity.ok(courseService.removeType(idCourse, idType));
    }

    @PatchMapping("{idCourse}/set-cuisine/{idType}")
    public ResponseEntity<?> setCuisine(@PathVariable Long idCourse, @PathVariable Long idType) {
        return ResponseEntity.ok(courseService.setCuisine(idCourse, idType));
    }

    @PatchMapping("{idCourse}/remove-cuisine/{idType}")
    public ResponseEntity<?> removeCuisine(@PathVariable Long idCourse, @PathVariable Long idType) {
        return ResponseEntity.ok(courseService.removeCuisine(idCourse, idType));
    }
}
