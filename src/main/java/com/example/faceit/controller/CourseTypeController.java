package com.example.faceit.controller;

import com.example.faceit.dtos.CourseTypeDto;
import com.example.faceit.service.CourseTypeService;
import com.example.faceit.validator.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/course/types")
public class CourseTypeController {

    private final CourseTypeService courseTypeService;
    private final ResponseErrorValidation responseErrorValidation;

    public CourseTypeController(CourseTypeService courseTypeService, ResponseErrorValidation responseErrorValidation) {
        this.courseTypeService = courseTypeService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @GetMapping
    public ResponseEntity<?> getAllCourseTypes() {
        return ResponseEntity.ok(courseTypeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCourseTypeById(@PathVariable Long id) {
        return ResponseEntity.ok(courseTypeService.findById(id));
    }

    @PostMapping
    public ResponseEntity<?> createCourseType(@Valid @RequestBody CourseTypeDto courseTypeDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(courseTypeService.createCourseType(courseTypeDto));
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> allowUpdateCourseType(@PathVariable Long id, @Valid @RequestBody CourseTypeDto courseTypeDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return responseErrorValidation.mapValidationService(bindingResult);
        }
        return ResponseEntity.ok(courseTypeService.putCourseType(courseTypeDto,id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourseType(@PathVariable Long id) {
        return courseTypeService.deleteCourseType(id);
    }
}
