package com.example.faceit.repository;

import com.example.faceit.model.OrderDrink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDrinkRepository extends JpaRepository<OrderDrink, Long> {
}
