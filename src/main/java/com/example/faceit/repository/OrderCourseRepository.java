package com.example.faceit.repository;

import com.example.faceit.model.Order;
import com.example.faceit.model.OrderCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderCourseRepository extends JpaRepository<OrderCourse, Long> {
    Optional<OrderCourse> findByOrderIdAndCourseId(Long orderId, Long courseId);
}
