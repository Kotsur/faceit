package com.example.faceit.repository;

import com.example.faceit.model.Additive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdditiveRepository extends JpaRepository<Additive, Long> {
}
