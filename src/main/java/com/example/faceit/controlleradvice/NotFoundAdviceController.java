package com.example.faceit.controlleradvice;

import com.example.faceit.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class NotFoundAdviceController {

    @ExceptionHandler(AdditiveNotFoundException.class)
    ResponseEntity<?> additiveNotFoundHandler(AdditiveNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(DrinkNotFoundException.class)
    ResponseEntity<?> drinkNotFoundHandler(DrinkNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(CourseTypeNotFoundException.class)
    ResponseEntity<?> courseTypeNotFoundHandler(CourseTypeNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(CuisineTypeNotFoundException.class)
    ResponseEntity<?> cuisineTypeNotFoundHandler(CuisineTypeNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(CourseNotFoundException.class)
    ResponseEntity<?> courseNotFoundHandler(CourseNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    ResponseEntity<?> orderNotFoundHandler(OrderNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }

    @ExceptionHandler(OrderCourseNotFoundException.class)
    ResponseEntity<?> orderCourseNotFoundHandler(OrderCourseNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }
    @ExceptionHandler(OrderDrinkNotFoundException.class)
    ResponseEntity<?> orderDrinkNotFoundHandler(OrderDrinkNotFoundException ex) {
        Map<String, Object> model = new HashMap<>();
        model.put("message", ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(model);
    }


}
