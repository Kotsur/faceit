package com.example.faceit.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity(name = "order_course")
@Data
@NoArgsConstructor
public class OrderCourse {
    public OrderCourse(Course course) {
        this.course = course;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private Course course;

    @ManyToOne
    private Order order;
}
