package com.example.faceit.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "course")
@Data
@NoArgsConstructor
//Страви
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, unique = true)
    private String name;
    @PositiveOrZero
    private Double price;


    @ManyToOne
    @JoinColumn(name = "course_type_id")
    private CourseType courseType;

    @ManyToOne
    @JoinColumn(name = "cousine_id")
    private CuisineType cuisineType;
}
