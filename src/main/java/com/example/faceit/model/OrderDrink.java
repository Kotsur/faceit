package com.example.faceit.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity(name = "order_drink")
@Data
@NoArgsConstructor
public class OrderDrink {

    public OrderDrink(Drink drink) {
        this.drink = drink;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private Drink drink;
    @OneToMany
    private List<Additive> additives = new ArrayList<>();

    @ManyToOne
    private Order order;
}
