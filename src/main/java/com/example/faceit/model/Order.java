package com.example.faceit.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity(name = "orders")
@Data
@NoArgsConstructor
//Замовлення
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany
    private List<OrderCourse> orderCourses = new ArrayList<>();
    @OneToMany
    private List<OrderDrink> orderDrinks = new ArrayList<>();
//    @ManyToMany
//    @JoinTable(name = "orders_course",
//            joinColumns = @JoinColumn(name = "order_id"),
//            inverseJoinColumns = @JoinColumn(name = "course_id"))
//    private List<Course> courses;
//
//    @ManyToMany
//    @JoinTable(name = "orders_drink",
//            joinColumns = @JoinColumn(name = "order_id"),
//            inverseJoinColumns = @JoinColumn(name = "drink_id"))
//    private List<Drink> drinks;
}
