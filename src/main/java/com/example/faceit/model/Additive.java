package com.example.faceit.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "additive")
@Data
@NoArgsConstructor
@Table(indexes = @Index(columnList = "id"))

//Додатки до напоїв
public class Additive {
    public Additive(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne
    private Drink drink;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Additive additive = (Additive) o;
        return Objects.equals(id, additive.id) && Objects.equals(name, additive.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
