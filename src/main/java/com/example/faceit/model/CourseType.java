package com.example.faceit.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Entity(name = "course_type")
@Data
@NoArgsConstructor
//Тип страви
public class CourseType {
    public CourseType(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "courseType")
    private List<Course> courseList = new ArrayList<>();
}
