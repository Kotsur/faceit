package com.example.faceit.service;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.exception.AdditiveNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Additive;
import com.example.faceit.repository.AdditiveRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AdditiveServiceTest {

    @Mock
    private Mapper mapper;

    @Mock
    private AdditiveRepository additiveRepository;

    @InjectMocks
    private AdditiveService additiveService;



    @Test
    void findAll() {
        List<Additive> additives = getAdditives();

        when(additiveRepository.findAll()).thenReturn(additives);

        List<AdditiveDto> expectedDtos = getAdditiveDto();

        when(mapper.toDto(additives.get(0))).thenReturn(expectedDtos.get(0));
        when(mapper.toDto(additives.get(1))).thenReturn(expectedDtos.get(1));

        // Act
        List<AdditiveDto> result = additiveService.findAll();

        // Assert
        assertEquals(expectedDtos.size(), result.size());
    }

    @Test
    void testFindById_WhenAdditiveExists() {
        // Arrange
        Long id = 1L;
        Additive additive = getAdditives().get(0);
        AdditiveDto expectedDto = getAdditiveDto().get(0);

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.of(additive));
        when(mapper.toDto(additive)).thenReturn(expectedDto);

        // Act
        AdditiveDto result = additiveService.findById(id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testFindById_WhenAdditiveDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(AdditiveNotFoundException.class, () -> additiveService.findById(id));
        verify(additiveRepository, times(1)).findById(id);
    }

    @Test
    void testCreateAdditive() {
        // Arrange
        AdditiveDto inputDto = getAdditiveDto().get(0);
        Additive entity = getAdditives().get(0);
        Additive savedEntity = getAdditives().get(1);
        AdditiveDto expectedDto = getAdditiveDto().get(1);

        when(mapper.toEntity(inputDto)).thenReturn(entity);
        when(additiveRepository.save(entity)).thenReturn(savedEntity);
        when(mapper.toDto(savedEntity)).thenReturn(expectedDto);

        // Act
        AdditiveDto result = additiveService.creteAdditive(inputDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testUpdateAdditive_WhenAdditiveExists() {
        // Arrange
        Long id = 2L;
        AdditiveDto inputDto = getAdditiveDto().get(0);
        Additive additive = getAdditives().get(0);
        Additive savedAdditive = getAdditives().get(1);
        AdditiveDto expectedDto = getAdditiveDto().get(1);

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.of(additive));
        when(mapper.toEntity(inputDto)).thenReturn(additive);
        when(additiveRepository.save(additive)).thenReturn(savedAdditive);
        when(mapper.toDto(savedAdditive)).thenReturn(expectedDto);

        // Act
        AdditiveDto result = additiveService.updateAdditive(inputDto, id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
        assertEquals(id, savedAdditive.getId());
    }

    @Test
    void testUpdateAdditive_WhenAdditiveDoesNotExist() {
        // Arrange
        Long id = 1L;
        AdditiveDto inputDto = getAdditiveDto().get(0);

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(AdditiveNotFoundException.class, () -> additiveService.updateAdditive(inputDto, id));
    }

    @Test
    void testDelete_WhenAdditiveExists() {
        // Arrange
        Long id = 1L;
        Additive additive = getAdditives().get(0);

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.of(additive));

        // Act
        ResponseEntity<?> response = additiveService.delete(id);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(additiveRepository, times(1)).delete(additive);
    }

    @Test
    void testDelete_WhenAdditiveDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(additiveRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(AdditiveNotFoundException.class, () -> additiveService.delete(id));
    }

    private List<Additive> getAdditives() {
        List<Additive> additives = new ArrayList<>();
        Additive additive1 = new Additive();
        Additive additive2 = new Additive();
        additive1.setId(1l);
        additive2.setId(2l);
        additive1.setName("Additive 1");
        additive2.setName("Additive 2");
        additives.add(additive1);
        additives.add(additive2);
        return additives;
    }

    private List<AdditiveDto> getAdditiveDto() {
        List<AdditiveDto> additives = new ArrayList<>();
        AdditiveDto additive1 = new AdditiveDto(1L, "Additive 1");
        AdditiveDto additive2 = new AdditiveDto(2L, "Additive 2");
        additives.add(additive1);
        additives.add(additive2);
        return additives;
    }
}