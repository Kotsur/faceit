package com.example.faceit.service;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.dtos.DrinkDto;
import com.example.faceit.dtos.DrinkDtoPost;
import com.example.faceit.exception.DrinkNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Additive;
import com.example.faceit.model.Drink;
import com.example.faceit.repository.DrinkRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DrinkServiceTest {
    @Mock
    private DrinkRepository drinkRepository;
    @Mock
    private Mapper mapper;
    @Mock
    private AdditiveService additiveService;
    @InjectMocks
    private DrinkService drinkService;

    @Test
    void testGetDrinks() {
        // Arrange
        List<Drink> drinks = drinks();

        when(drinkRepository.findAll()).thenReturn(drinks);


        List<DrinkDto> expectedDtos = drinkDtos();

        when(mapper.toDto(drinks.get(0))).thenReturn(expectedDtos.get(0));
        when(mapper.toDto(drinks.get(1))).thenReturn(expectedDtos.get(1));

        // Act
        List<DrinkDto> result = drinkService.getDrinks();

        // Assert
        assertEquals(expectedDtos.size(), result.size());
        for (int i = 0; i < expectedDtos.size(); i++) {
            assertEquals(expectedDtos.get(i), result.get(i));
        }
    }

    @Test
    void testCreateDrink() {
        // Arrange
        DrinkDtoPost inputDto = new DrinkDtoPost("test drink1", 10.0);
        Drink entity = drinks().get(0);
        Drink savedEntity = drinks().get(1);
        DrinkDto expectedDto = drinkDtos().get(0);

        when(mapper.toEntity(inputDto)).thenReturn(entity);
        when(drinkRepository.save(entity)).thenReturn(savedEntity);
        when(mapper.toDto(savedEntity)).thenReturn(expectedDto);

        // Act
        DrinkDto result = drinkService.creteDrink(inputDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testPutDrink_WhenDrinkExists() {
        // Arrange
        Long id = 1L;
        DrinkDtoPost inputDto = new DrinkDtoPost("test drink1", 10.0);
        ;
        Drink drink = drinks().get(1);
        Drink savedDrink = drinks().get(0);
        DrinkDto expectedDto = drinkDtos().get(0);

        when(drinkRepository.findById(id)).thenReturn(java.util.Optional.of(drink));
        when(mapper.toEntity(inputDto)).thenReturn(drink);
        when(drinkRepository.save(drink)).thenReturn(savedDrink);
        when(mapper.toDto(savedDrink)).thenReturn(expectedDto);

        // Act
        DrinkDto result = drinkService.putDrink(inputDto, id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
        assertEquals(id, savedDrink.getId());
    }

    @Test
    void testPutDrink_WhenDrinkDoesNotExist() {
        // Arrange
        Long id = 1L;
        DrinkDtoPost inputDto = new DrinkDtoPost("test drink1", 10.0);

        when(drinkRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(DrinkNotFoundException.class, () -> drinkService.putDrink(inputDto, id));
        verify(drinkRepository, times(1)).findById(id);
    }
    @Test
    public void testPatchDrink_WhenDrinkExists_ShouldReturnUpdatedDrinkDto() {
        // Arrange
        Long id = 1L;
        DrinkDtoPost drinkDto = new DrinkDtoPost("test drink1", 10.0);
        Drink drink = drinks().get(0);
        when(drinkRepository.findById(id)).thenReturn(Optional.of(drink));
        when(drinkRepository.save(drink)).thenReturn(drink);
        when(mapper.toDto(drink)).thenReturn(drinkDtos().get(0));

        // Act
        DrinkDto result = drinkService.patchDrink(drinkDto, id);

        // Assert
        verify(drinkRepository, times(1)).findById(id);
        verify(drinkRepository, times(1)).save(any(Drink.class));
        verify(mapper, times(1)).toDto(any(Drink.class));
        assertEquals(drink.getName(), result.getName());
        assertEquals(drink.getPrice(), result.getPrice());
    }

    @Test
    public void testPatchDrink_WhenDrinkNotFound_ShouldThrowException() {
        // Arrange
        Long id = 1L;
        DrinkDtoPost drinkDto = new DrinkDtoPost("test drink1", 10.0);
        when(drinkRepository.findById(id)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(DrinkNotFoundException.class, () -> drinkService.patchDrink(drinkDto, id));
        verify(drinkRepository, times(1)).findById(id);
        verify(drinkRepository, never()).save(any(Drink.class));
        verify(mapper, never()).toDto(any(Drink.class));
    }

    @Test
    void testAddAdditive() {
        Long drinkId = 1L;
        Long additiveId = 2L;

        Drink drink = new Drink();
        drink.setId(drinkId);
        Additive additive = new Additive("Test additive");
        additive.setId(additiveId);
        drink.setAdditives(List.of(additive));
        AdditiveDto additiveDto = new AdditiveDto(2L, "Test additive");

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.of(drink));
        when(additiveService.findById(additiveId)).thenReturn(additiveDto);
        when(mapper.toEntity(any(AdditiveDto.class))).thenReturn(additive);
        when(drinkRepository.save(any(Drink.class))).thenReturn(drink);
        when(mapper.toDto(any(Drink.class))).thenReturn(drinkDtos().get(0));

        DrinkDto result = drinkService.addAdditive(drinkId, additiveId);

        assertEquals(drinkDtos().get(0), result);
        ArgumentCaptor<Drink> drinkCaptor = ArgumentCaptor.forClass(Drink.class);
        verify(drinkRepository).save(drinkCaptor.capture());
        Drink savedDrink = drinkCaptor.getValue();

        assertEquals(1, savedDrink.getAdditives().size());
        assertEquals(additiveId, savedDrink.getAdditives().get(0).getId());
    }
    @Test
    void testAddAdditiveDrinkNotFound() {
        Long drinkId = 1L;
        Long additiveId = 2L;

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.empty());

        assertThrows(DrinkNotFoundException.class, () -> {
            drinkService.addAdditive(drinkId, additiveId);
        });

        verify(drinkRepository, never()).save(any(Drink.class));
    }

    @Test
    void testRemoveAdditive() {
        Long drinkId = 1L;
        Long additiveId = 2L;

        Additive additive = new Additive();
        additive.setId(additiveId);

        Drink drink = new Drink();
        drink.setId(drinkId);
        List<Additive> additives = new ArrayList<>();
        additives.add(additive);
        drink.setAdditives(additives);

        AdditiveDto additiveDto = new AdditiveDto(2L, "Test additive");
        DrinkDto expectedDrinkDto = drinkDtos().get(0);

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.of(drink));
        when(additiveService.findById(additiveId)).thenReturn(additiveDto);
        when(mapper.toEntity(additiveDto)).thenReturn(additive);
        when(drinkRepository.save(any(Drink.class))).thenReturn(drink);
        when(mapper.toDto(drink)).thenReturn(expectedDrinkDto);

        DrinkDto result = drinkService.removeAdditive(drinkId, additiveId);

        assertEquals(expectedDrinkDto, result);

        ArgumentCaptor<Drink> drinkCaptor = ArgumentCaptor.forClass(Drink.class);
        verify(drinkRepository).save(drinkCaptor.capture());
        Drink savedDrink = drinkCaptor.getValue();

        assertEquals(0, savedDrink.getAdditives().size());
    }

    @Test
    void testRemoveAdditiveDrinkNotFound() {
        Long drinkId = 1L;
        Long additiveId = 2L;

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.empty());

        assertThrows(DrinkNotFoundException.class, () -> {
            drinkService.removeAdditive(drinkId, additiveId);
        });

        verify(drinkRepository, never()).save(any(Drink.class));
    }
    @Test
    void testDelete() {
        Long drinkId = 1L;
        Drink drink = new Drink();
        drink.setId(drinkId);

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.of(drink));

        ResponseEntity<?> response = drinkService.delete(drinkId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(drinkRepository).delete(drink);
    }

    @Test
    void testDeleteDrinkNotFound() {
        Long drinkId = 1L;

        when(drinkRepository.findById(drinkId)).thenReturn(Optional.empty());

        assertThrows(DrinkNotFoundException.class, () -> {
            drinkService.delete(drinkId);
        });

        verify(drinkRepository, never()).delete(any(Drink.class));
    }


    private List<Drink> drinks() {
        Drink drink1 = new Drink();
        drink1.setId(1l);
        drink1.setName("test drink");
        drink1.setPrice(10.0);
        Drink drink2 = new Drink();
        drink2.setId(2l);
        drink2.setName("test drink2");
        drink2.setPrice(20.0);
        return List.of(drink1, drink2);
    }

    private List<DrinkDto> drinkDtos() {
        DrinkDto drinkDto1 = new DrinkDto(1L, "test drink1", 10.0, null);
        DrinkDto drinkDto2 = new DrinkDto(2L, "test drink2", 20.0, null);
        return List.of(drinkDto1, drinkDto2);
    }

}