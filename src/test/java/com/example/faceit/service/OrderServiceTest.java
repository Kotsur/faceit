package com.example.faceit.service;

import com.example.faceit.dtos.AdditiveDto;
import com.example.faceit.dtos.DrinkDto;
import com.example.faceit.dtos.OrderDrinkDto;
import com.example.faceit.dtos.OrderDto;
import com.example.faceit.exception.*;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.*;
import com.example.faceit.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @Mock
    private  OrderRepository orderRepository;
    @Mock
    private  Mapper mapper;
    @Mock
    private  CourseRepository courseRepository;
    @Mock
    private  DrinkRepository drinkRepository;
    @Mock
    private  AdditiveRepository additiveRepository;
    @Mock
    private  OrderCourseRepository orderCourseRepository;
    @Mock
    private  OrderDrinkRepository orderDrinkRepository;
    @InjectMocks
    private OrderService orderService;
    @Test
    void testGetAllOrders() {


        List<Order> orders = orders();
        List<OrderDto> orderDtos = orderDtos();

        // Configure mock behaviors
        when(orderRepository.findAll()).thenReturn(orders);
        when(mapper.toDto(orders.get(0))).thenReturn(orderDtos.get(0));
        when(mapper.toDto(orders.get(1))).thenReturn(orderDtos.get(1));

        // Call the service method
        List<OrderDto> result = orderService.getAllOrders();

        // Verify the interactions and the result
        assertEquals(orderDtos, result);
        verify(orderRepository, times(1)).findAll();
    }
    @Test
    void testFindOrderById() {
        Long orderId = 1L;
        Order order = orders().get(0);

        OrderDto orderDto = orderDtos().get(0);


        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.findOrderById(orderId);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).findById(orderId);
    }

    @Test
    void testFindOrderByIdNotFound() {
        Long orderId = 1L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.findOrderById(orderId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(mapper, never()).toDto(any(Order.class));
    }
    @Test
    void testCreateOrder() {
        OrderDto orderDto = orderDtos().get(0);

        Order order = orders().get(0);

        when(mapper.toEntity(orderDto)).thenReturn(order);
        when(orderRepository.save(order)).thenReturn(order);
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.createOrder(orderDto);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).save(order);
    }
    @Test
    void testDeleteOrder() {
        Long orderId = 1L;
        Order order =orders().get(0);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));

        ResponseEntity<?> response = orderService.deleteOrder(orderId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(orderRepository).findById(orderId);
        verify(orderRepository).delete(order);
    }

    @Test
    void testDeleteOrderNotFound() {
        Long orderId = 1L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.deleteOrder(orderId);
        });

        verify(orderRepository).findById(orderId);
        verify(orderRepository, never()).delete(orders().get(0));
    }

    @Test
    void testAddCourse() {
        Long orderId = 1L;
        Long courseId = 2L;
        Order order = orders().get(0);
        Course course = new Course();
        course.setId(courseId);
        OrderCourse orderCourse = new OrderCourse(course);

        OrderDto orderDto = orderDtos().get(0);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(orderRepository.save(order)).thenReturn(order);
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.addCourse(orderId, courseId);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).findById(orderId);
        verify(courseRepository, times(1)).findById(courseId);
        verify(orderRepository, times(1)).save(order);
        verify(mapper, times(1)).toDto(order);
        assertEquals(1, order.getOrderCourses().size());
        assertEquals(orderCourse, order.getOrderCourses().iterator().next());
    }

    @Test
    void testAddCourseOrderNotFound() {
        Long orderId = 1L;
        Long courseId = 2L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.addCourse(orderId, courseId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(courseRepository, never()).findById(anyLong());
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testAddCourseCourseNotFound() {
        Long orderId = 1L;
        Long courseId = 2L;
        Order order = new Order();
        order.setId(orderId);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        assertThrows(CourseNotFoundException.class, () -> {
            orderService.addCourse(orderId, courseId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(courseRepository, times(1)).findById(courseId);
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }
    @Test
    void testRemoveCourse() {
        Long orderId = 1L;
        Long courseId = 2L;
        Order order = new Order();
        order.setId(orderId);
        Course course = new Course();
        course.setId(courseId);
        OrderCourse orderCourse = new OrderCourse();
        orderCourse.setCourse(course);

        order.getOrderCourses().add(orderCourse);

        OrderDto orderDto = orderDtos().get(0);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(orderCourseRepository.findByOrderIdAndCourseId(orderId, courseId)).thenReturn(Optional.of(orderCourse));
        when(orderRepository.save(order)).thenReturn(order);
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.removeCourse(orderId, courseId);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).findById(orderId);
        verify(orderCourseRepository, times(1)).findByOrderIdAndCourseId(orderId, courseId);
        verify(orderRepository, times(1)).save(order);
      }

    @Test
    void testRemoveCourseOrderNotFound() {
        Long orderId = 1L;
        Long courseId = 2L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.removeCourse(orderId, courseId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderCourseRepository, never()).findByOrderIdAndCourseId(anyLong(), anyLong());
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testRemoveCourseNotFound() {
        Long orderId = 1L;
        Long courseId = 2L;
        Order order = new Order();
        order.setId(orderId);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(orderCourseRepository.findByOrderIdAndCourseId(orderId, courseId)).thenReturn(Optional.empty());

        assertThrows(OrderCourseNotFoundException.class, () -> {
            orderService.removeCourse(orderId, courseId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderCourseRepository, times(1)).findByOrderIdAndCourseId(orderId, courseId);
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }
    @Test
    void testAddDrink() {
        Long orderId = 1L;
        Long drinkId = 2L;
        Order order = new Order();
        order.setId(orderId);
        Drink drink = new Drink();
        drink.setId(drinkId);
        OrderDrink orderDrink = new OrderDrink(drink);
        OrderDto orderDto = orderDtos().get(0);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(drinkRepository.findById(drinkId)).thenReturn(Optional.of(drink));
        when(orderRepository.save(order)).thenReturn(order);
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.addDrink(orderId, drinkId);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).findById(orderId);
        verify(drinkRepository, times(1)).findById(drinkId);
        verify(orderRepository, times(1)).save(order);
        verify(mapper, times(1)).toDto(order);
        assertEquals(1, order.getOrderDrinks().size());
        assertEquals(orderDrink, order.getOrderDrinks().iterator().next());
    }

    @Test
    void testAddDrinkOrderNotFound() {
        Long orderId = 1L;
        Long drinkId = 2L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.addDrink(orderId, drinkId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(drinkRepository, never()).findById(anyLong());
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testAddDrinkDrinkNotFound() {
        Long orderId = 1L;
        Long drinkId = 2L;
        Order order = new Order();
        order.setId(orderId);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(drinkRepository.findById(drinkId)).thenReturn(Optional.empty());

        assertThrows(DrinkNotFoundException.class, () -> {
            orderService.addDrink(orderId, drinkId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(drinkRepository, times(1)).findById(drinkId);
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testRemoveDrink() {
        Long orderId = 1L;
        Long orderDrinkId = 2L;
        Order order = new Order();
        order.setId(orderId);
        OrderDrink orderDrink = new OrderDrink();
        orderDrink.setId(orderDrinkId);
        order.getOrderDrinks().add(orderDrink);
        OrderDto orderDto = orderDtos().get(0);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.of(orderDrink));
        when(orderRepository.save(order)).thenReturn(order);
        when(mapper.toDto(order)).thenReturn(orderDto);

        OrderDto result = orderService.removeDrink(orderId, orderDrinkId);

        assertEquals(orderDto, result);
        verify(orderRepository, times(1)).findById(orderId);
        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(orderRepository, times(1)).save(order);
        assertEquals(0, order.getOrderDrinks().size());
    }

    @Test
    void testRemoveDrinkOrderNotFound() {
        Long orderId = 1L;
        Long orderDrinkId = 2L;

        when(orderRepository.findById(orderId)).thenReturn(Optional.empty());

        assertThrows(OrderNotFoundException.class, () -> {
            orderService.removeDrink(orderId, orderDrinkId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderDrinkRepository, never()).findById(anyLong());
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testRemoveDrinkOrderDrinkNotFound() {
        Long orderId = 1L;
        Long orderDrinkId = 2L;
        Order order = new Order();
        order.setId(orderId);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.empty());

        assertThrows(OrderDrinkNotFoundException.class, () -> {
            orderService.removeDrink(orderId, orderDrinkId);
        });

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(orderRepository, never()).save(any(Order.class));
        verify(mapper, never()).toDto(any(Order.class));
    }

    @Test
    void testAddAdditive() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;
        OrderDrink orderDrink = new OrderDrink(drinks().get(0));
        Additive additive =  getAdditives().get(0);

        OrderDrinkDto orderDrinkDto= new OrderDrinkDto(1L, drinkDtos().get(0), getAdditiveDto());


        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.of(orderDrink));
        when(additiveRepository.findById(additiveId)).thenReturn(Optional.of(additive));
        when(orderDrinkRepository.save(orderDrink)).thenReturn(orderDrink);
        when(mapper.toDto(orderDrink)).thenReturn(orderDrinkDto);

        OrderDrinkDto result = orderService.addAdditive(orderDrinkId, additiveId);

        assertEquals(orderDrinkDto, result);
        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, times(1)).findById(additiveId);
        verify(orderDrinkRepository, times(1)).save(orderDrink);
        verify(mapper, times(1)).toDto(orderDrink);
    }

    @Test
    void testAddAdditiveOrderDrinkNotFound() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;

        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.empty());

        assertThrows(OrderDrinkNotFoundException.class, () -> {
            orderService.addAdditive(orderDrinkId, additiveId);
        });

        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, never()).findById(anyLong());
        verify(orderDrinkRepository, never()).save(any());
    }

    @Test
    void testAddAdditiveAdditiveNotFound() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;
        OrderDrink orderDrink = new OrderDrink();
        orderDrink.setId(orderDrinkId);

        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.of(orderDrink));
        when(additiveRepository.findById(additiveId)).thenReturn(Optional.empty());

        assertThrows(AdditiveNotFoundException.class, () -> {
            orderService.addAdditive(orderDrinkId, additiveId);
        });

        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, times(1)).findById(additiveId);
        verify(orderDrinkRepository, never()).save(any());
    }

    @Test
    void testRemoveAdditive() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;
        OrderDrink orderDrink = new OrderDrink(drinks().get(0));
        Additive additive =  getAdditives().get(0);

        OrderDrinkDto orderDrinkDto= new OrderDrinkDto(1L, drinkDtos().get(0), getAdditiveDto());

        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.of(orderDrink));
        when(additiveRepository.findById(additiveId)).thenReturn(Optional.of(additive));
        when(orderDrinkRepository.save(orderDrink)).thenReturn(orderDrink);
        when(mapper.toDto(orderDrink)).thenReturn(orderDrinkDto);

        OrderDrinkDto result = orderService.removeAdditive(orderDrinkId, additiveId);

        assertEquals(orderDrinkDto, result);
        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, times(1)).findById(additiveId);
        verify(orderDrinkRepository, times(1)).save(orderDrink);
        verify(mapper, times(1)).toDto(orderDrink);
    }

    @Test
    void testRemoveAdditiveOrderDrinkNotFound() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;

        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.empty());

        assertThrows(OrderDrinkNotFoundException.class, () -> {
            orderService.removeAdditive(orderDrinkId, additiveId);
        });

        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, never()).findById(anyLong());
        verify(orderDrinkRepository, never()).save(any());
    }

    @Test
    void testRemoveAdditiveAdditiveNotFound() {
        Long orderDrinkId = 1L;
        Long additiveId = 2L;
        OrderDrink orderDrink = new OrderDrink();
        orderDrink.setId(orderDrinkId);

        when(orderDrinkRepository.findById(orderDrinkId)).thenReturn(Optional.of(orderDrink));
        when(additiveRepository.findById(additiveId)).thenReturn(Optional.empty());

        assertThrows(AdditiveNotFoundException.class, () -> {
            orderService.removeAdditive(orderDrinkId, additiveId);
        });

        verify(orderDrinkRepository, times(1)).findById(orderDrinkId);
        verify(additiveRepository, times(1)).findById(additiveId);
        verify(orderDrinkRepository, never()).save(any());
    }

    private List<Order> orders () {
        Order order = new Order();
        order.setId(1L);
        Order order2 = new Order();
        order2.setId(2L);
        return List.of(order, order2);
    }
    private List<OrderDto> orderDtos () {
        return orders().stream().map(mapper::toDto).toList();
    }

    private List<AdditiveDto> getAdditiveDto() {
        List<AdditiveDto> additives = new ArrayList<>();
        AdditiveDto additive1 = new AdditiveDto(1L, "Additive 1");
        AdditiveDto additive2 = new AdditiveDto(2L, "Additive 2");
        additives.add(additive1);
        additives.add(additive2);
        return additives;
    }

    private List<Additive> getAdditives() {
        List<Additive> additives = new ArrayList<>();
        Additive additive1 = new Additive();
        Additive additive2 = new Additive();
        additive1.setId(1l);
        additive2.setId(2l);
        additive1.setName("Additive 1");
        additive2.setName("Additive 2");
        additives.add(additive1);
        additives.add(additive2);
        return additives;
    }
    private List<Drink> drinks() {
        Drink drink1 = new Drink();
        drink1.setId(1l);
        drink1.setName("test drink");
        drink1.setPrice(10.0);
        Drink drink2 = new Drink();
        drink2.setId(2l);
        drink2.setName("test drink2");
        drink2.setPrice(20.0);
        return List.of(drink1, drink2);
    }

    private List<DrinkDto> drinkDtos() {
        DrinkDto drinkDto1 = new DrinkDto(1L, "test drink1", 10.0, null);
        DrinkDto drinkDto2 = new DrinkDto(2L, "test drink2", 20.0, null);
        return List.of(drinkDto1, drinkDto2);
    }
}