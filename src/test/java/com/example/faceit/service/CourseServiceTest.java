package com.example.faceit.service;

import com.example.faceit.dtos.CourseDto;
import com.example.faceit.exception.CourseNotFoundException;
import com.example.faceit.exception.CourseTypeNotFoundException;
import com.example.faceit.exception.CuisineTypeNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.Course;
import com.example.faceit.model.CourseType;
import com.example.faceit.model.CuisineType;
import com.example.faceit.repository.CourseRepository;
import com.example.faceit.repository.CourseTypeRepository;
import com.example.faceit.repository.CuisineTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)

class CourseServiceTest {
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private CourseTypeRepository courseTypeRepository;
    @Mock
    private CuisineTypeRepository cuisineRepository;
    @Mock
    private Mapper mapper;
    @InjectMocks
    private CourseService courseService;

    @Test
    void findAll() {
        List<Course> courses = getCourses();
        List<CourseDto> courseDtos = getCourseDtos();
        when(courseRepository.findAll()).thenReturn(courses);
        when(mapper.toDto(courses.get(0))).thenReturn(courseDtos.get(0));
        when(mapper.toDto(courses.get(1))).thenReturn(courseDtos.get(1));

        List<CourseDto> result = courseService.findAll();

        assertEquals(courseDtos, result);
        verify(courseRepository, times(1)).findAll();
        verify(mapper, times(1)).toDto(courses.get(0));
        verify(mapper, times(1)).toDto(courses.get(1));

    }

    @Test
    public void testFindById_CourseFound() {
        Long courseId = 1L;
        Course course = getCourses().get(0);
        CourseDto courseDto = getCourseDtos().get(0);

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(mapper.toDto(course)).thenReturn(courseDto);

        CourseDto result = courseService.findById(courseId);

        assertEquals(courseDto, result);
        verify(courseRepository, times(1)).findById(courseId);
        verify(mapper, times(1)).toDto(course);
    }

    @Test
    public void testFindById_CourseNotFound() {
        Long courseId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        assertThrows(CourseNotFoundException.class, () -> {
            courseService.findById(courseId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    public void testCreateCourse() {
        Course course = getCourses().get(0);
        CourseDto courseDto = getCourseDtos().get(0);

        when(mapper.toEntity(courseDto)).thenReturn(course);
        when(courseRepository.save(course)).thenReturn(course);
        when(mapper.toDto(course)).thenReturn(courseDto);

        CourseDto result = courseService.createCourse(courseDto);

        assertEquals(courseDto, result);
        verify(mapper, times(1)).toEntity(courseDto);
        verify(courseRepository, times(1)).save(course);
        verify(mapper, times(1)).toDto(course);
    }

    @Test
    public void testPutCourse_CourseExists() {
        Long courseId = 1L;
        Course course = getCourses().get(0);
        CourseDto courseDto = getCourseDtos().get(0);
        Course updatedCourse = getCourses().get(1);

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(mapper.toEntity(courseDto)).thenReturn(course);
        when(courseRepository.save(course)).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(courseDto);

        CourseDto result = courseService.putCourse(courseDto, courseId);

        assertEquals(courseDto, result);
        verify(courseRepository, times(1)).findById(courseId);
        verify(mapper, times(1)).toEntity(courseDto);
        verify(courseRepository, times(1)).save(course);
        verify(mapper, times(1)).toDto(updatedCourse);
    }

    @Test
    public void testPutCourse_CourseNotFound() {
        Long courseId = 1L;
        CourseDto courseDto = getCourseDtos().get(1);

        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        assertThrows(CourseNotFoundException.class, () -> {
            courseService.putCourse(courseDto, courseId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(mapper, times(0)).toEntity(any(CourseDto.class));
        verify(courseRepository, times(0)).save(any(Course.class));
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void patchCourse_WithValidData_ShouldReturnUpdatedCourseDto() {
        // Arrange
        Long courseId = 1L;
        String updatedName = "test1";
        Double updatedPrice = 10.0;

        CourseDto inputDto = getCourseDtos().get(0);


        Course existingCourse = new Course();
        existingCourse.setId(courseId);
        existingCourse.setName("Original Name");
        existingCourse.setPrice(50.0);

        Course updatedCourse = new Course();
        updatedCourse.setId(courseId);
        updatedCourse.setName(updatedName);
        updatedCourse.setPrice(updatedPrice);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(existingCourse));
        when(courseRepository.save(any(Course.class))).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(inputDto);

        CourseDto resultDto = courseService.patchCourse(inputDto, courseId);

        assertNotNull(resultDto);
        assertEquals(updatedName, resultDto.getName());
        assertEquals(updatedPrice, resultDto.getPrice());
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseRepository, times(1)).save(existingCourse);
        verify(mapper, times(1)).toDto(updatedCourse);
    }

    @Test
    void patchCourse_WithNonExistingCourse_ShouldThrowException() {
        Long courseId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        assertThrows(CourseNotFoundException.class, () -> {
            courseService.patchCourse(getCourseDtos().get(0), courseId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseRepository, times(0)).save(any());
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void deleteCourse_WithValidId_ShouldReturnNoContent() {
        Long courseId = 1L;
        Course existingCourse = new Course();
        existingCourse.setId(courseId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(existingCourse));

        ResponseEntity<?> responseEntity = courseService.deleteCourse(courseId);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseRepository, times(1)).delete(existingCourse);
    }

    @Test
    void deleteCourse_WithInvalidId_ShouldThrowException() {
        Long courseId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        assertThrows(CourseNotFoundException.class, () -> {
            courseService.deleteCourse(courseId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseRepository, times(0)).delete(any());
    }

    @Test
    void setType_WithValidIds_ShouldReturnUpdatedCourseDto() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        CourseType courseType = new CourseType("Original Name");
        courseType.setId(courseTypeId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(courseTypeRepository.findById(courseTypeId)).thenReturn(java.util.Optional.of(courseType));

        Course updatedCourse = new Course();
        updatedCourse.setId(courseId);
        updatedCourse.setCourseType(courseType);

        CourseDto updatedDto = getCourseDtos().get(0);

        when(courseRepository.save(course)).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(updatedDto);

        // Act
        CourseDto resultDto = courseService.setType(courseId, courseTypeId);

        // Assert
        assertNotNull(resultDto);
        assertEquals(updatedDto, resultDto);
        assertEquals(courseType, updatedCourse.getCourseType());
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(1)).findById(courseTypeId);
        verify(courseRepository, times(1)).save(course);
        verify(mapper, times(1)).toDto(updatedCourse);
    }

    @Test
    void setType_WithInvalidCourseId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> {
            courseService.setType(courseId, courseTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(0)).findById(any());
        verify(courseRepository, times(0)).save(any());
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void setType_WithInvalidCourseTypeId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(courseTypeRepository.findById(courseTypeId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseTypeNotFoundException.class, () -> {
            courseService.setType(courseId, courseTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(1)).findById(courseTypeId);
        verify(courseRepository, times(0)).save(any());
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void removeType_WithValidIds_ShouldReturnUpdatedCourseDto() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);
        CourseType courseType = new CourseType();
        courseType.setId(courseTypeId);
        course.setCourseType(courseType);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(courseTypeRepository.findById(courseTypeId)).thenReturn(java.util.Optional.of(courseType));

        Course updatedCourse = new Course();
        updatedCourse.setId(courseId);
        updatedCourse.setCourseType(null);

        CourseDto updatedDto = getCourseDtos().get(0);

        when(courseRepository.save(course)).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(updatedDto);

        // Act
        CourseDto resultDto = courseService.removeType(courseId, courseTypeId);

        // Assert
        assertNotNull(resultDto);
        assertEquals(updatedDto, resultDto);
        assertNull(updatedCourse.getCourseType());
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(1)).findById(courseTypeId);

    }

    @Test
    void removeType_WithInvalidCourseId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> {
            courseService.removeType(courseId, courseTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(0)).findById(any());
        verify(courseRepository, times(0)).save(any());
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void removeType_WithInvalidCourseTypeId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long courseTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(courseTypeRepository.findById(courseTypeId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseTypeNotFoundException.class, () -> {
            courseService.removeType(courseId, courseTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(courseTypeRepository, times(1)).findById(courseTypeId);
        verify(courseRepository, times(0)).save(any());
        verify(mapper, times(0)).toDto(any(Course.class));
    }

    @Test
    void setCuisine_WithValidIds_ShouldReturnUpdatedCourseDto() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        CuisineType cuisineType = new CuisineType();
        cuisineType.setId(cuisineTypeId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(cuisineRepository.findById(cuisineTypeId)).thenReturn(java.util.Optional.of(cuisineType));

        Course updatedCourse = new Course();
        updatedCourse.setId(courseId);
        updatedCourse.setCuisineType(cuisineType);

        CourseDto updatedDto = getCourseDtos().get(0);

        when(courseRepository.save(course)).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(updatedDto);

        // Act
        CourseDto resultDto = courseService.setCuisine(courseId, cuisineTypeId);

        // Assert
        assertNotNull(resultDto);
        assertEquals(updatedDto, resultDto);
        assertEquals(cuisineType, updatedCourse.getCuisineType());
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(1)).findById(cuisineTypeId);
        verify(courseRepository, times(1)).save(course);
        verify(mapper, times(1)).toDto(updatedCourse);
    }

    @Test
    void setCuisine_WithInvalidCourseId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> {
            courseService.setCuisine(courseId, cuisineTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(0)).findById(any());
        verify(courseRepository, times(0)).save(any());
    }

    @Test
    void setCuisine_WithInvalidCuisineTypeId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(cuisineRepository.findById(cuisineTypeId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CuisineTypeNotFoundException.class, () -> {
            courseService.setCuisine(courseId, cuisineTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(1)).findById(cuisineTypeId);
        verify(courseRepository, times(0)).save(any());
    }


    @Test
    void removeCuisine_WithValidIds_ShouldReturnUpdatedCourseDto() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        CuisineType cuisineType = new CuisineType();
        cuisineType.setId(cuisineTypeId);
        course.setCuisineType(cuisineType);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(cuisineRepository.findById(cuisineTypeId)).thenReturn(java.util.Optional.of(cuisineType));

        Course updatedCourse = new Course();
        updatedCourse.setId(courseId);

        CourseDto updatedDto = getCourseDtos().get(0);

        when(courseRepository.save(course)).thenReturn(updatedCourse);
        when(mapper.toDto(updatedCourse)).thenReturn(updatedDto);

        // Act
        CourseDto resultDto = courseService.removeCuisine(courseId, cuisineTypeId);

        // Assert
        assertNotNull(resultDto);
        assertEquals(updatedDto, resultDto);
        assertNull(updatedCourse.getCuisineType());
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(1)).findById(cuisineTypeId);
        verify(courseRepository, times(1)).save(course);
        verify(mapper, times(1)).toDto(updatedCourse);
    }

    @Test
    void removeCuisine_WithInvalidCourseId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> {
            courseService.removeCuisine(courseId, cuisineTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(0)).findById(any());
        verify(courseRepository, times(0)).save(any());
    }

    @Test
    void removeCuisine_WithInvalidCuisineTypeId_ShouldThrowException() {
        // Arrange
        Long courseId = 1L;
        Long cuisineTypeId = 1L;

        Course course = new Course();
        course.setId(courseId);

        when(courseRepository.findById(courseId)).thenReturn(java.util.Optional.of(course));
        when(cuisineRepository.findById(cuisineTypeId)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        assertThrows(CuisineTypeNotFoundException.class, () -> {
            courseService.removeCuisine(courseId, cuisineTypeId);
        });
        verify(courseRepository, times(1)).findById(courseId);
        verify(cuisineRepository, times(1)).findById(cuisineTypeId);
        verify(courseRepository, times(0)).save(any());
    }

    private List<Course> getCourses() {
        List<Course> courses = new ArrayList<>();
        Course course = new Course();
        course.setId(1L);
        course.setName("test1");
        courses.add(course);
        Course course2 = new Course();
        course2.setId(2L);
        course2.setName("test2");
        courses.add(course2);
        return courses;
    }

    private List<CourseDto> getCourseDtos() {
        List<CourseDto> courseDtos = new ArrayList<>();
        CourseDto course1 = new CourseDto(1L, "test1", 10.0, null, null);
        CourseDto course2 = new CourseDto(2L, "test2", 10.0, null, null);
        return List.of(course1, course2);
    }
}