package com.example.faceit.service;

import com.example.faceit.dtos.CourseTypeDto;
import com.example.faceit.exception.CourseTypeNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.CourseType;
import com.example.faceit.repository.CourseTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CourseTypeServiceTest {
    @Mock
    private  CourseTypeRepository courseTypeRepository;
    @Mock
    private  Mapper mapper;
    @InjectMocks
    private CourseTypeService courseTypeService;

    @Test
    void testFindAll() {

        List<CourseType> courseTypes =  getCourseTypes();

        when(courseTypeRepository.findAll()).thenReturn(courseTypes);

        List<CourseTypeDto> expectedDtos = getCourseTypeDtos();

        when(mapper.toDto(courseTypes.get(0))).thenReturn(expectedDtos.get(0));
        when(mapper.toDto(courseTypes.get(1))).thenReturn(expectedDtos.get(1));

        // Act
        List<CourseTypeDto> result = courseTypeService.findAll();

        // Assert
        assertEquals(expectedDtos.size(), result.size());
        for (int i = 0; i < expectedDtos.size(); i++) {
            assertEquals(expectedDtos.get(i), result.get(i));
        }
    }

    @Test
    void testFindById_WhenCourseTypeExists() {
        // Arrange
        Long id = 1L;
        CourseType courseType = getCourseTypes().get(0);
        CourseTypeDto expectedDto =getCourseTypeDtos().get(0);

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.of(courseType));
        when(mapper.toDto(courseType)).thenReturn(expectedDto);

        // Act
        CourseTypeDto result = courseTypeService.findById(id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testFindById_WhenCourseTypeDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CourseTypeNotFoundException.class, () -> courseTypeService.findById(id));
        verify(courseTypeRepository).findById(id);
    }

    @Test
    void testCreateCourseType() {
        // Arrange
        CourseTypeDto inputDto = getCourseTypeDtos().get(0);
        CourseType entity = getCourseTypes().get(0);
        CourseType savedEntity =getCourseTypes().get(1);
        CourseTypeDto expectedDto = getCourseTypeDtos().get(1);

        when(mapper.toEntity(inputDto)).thenReturn(entity);
        when(courseTypeRepository.save(entity)).thenReturn(savedEntity);
        when(mapper.toDto(savedEntity)).thenReturn(expectedDto);

        // Act
        CourseTypeDto result = courseTypeService.createCourseType(inputDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testPutCourseType_WhenCourseTypeExists() {
        // Arrange
        Long id = 2L;
        CourseTypeDto inputDto = getCourseTypeDtos().get(0);
        CourseType courseType = getCourseTypes().get(0);
        CourseType savedCourseType = getCourseTypes().get(1);
        CourseTypeDto expectedDto = getCourseTypeDtos().get(1);

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.of(courseType));
        when(mapper.toEntity(inputDto)).thenReturn(courseType);
        when(courseTypeRepository.save(courseType)).thenReturn(savedCourseType);
        when(mapper.toDto(savedCourseType)).thenReturn(expectedDto);

        // Act
        CourseTypeDto result = courseTypeService.putCourseType(inputDto, id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
        assertEquals(id, expectedDto.getId());
    }

    @Test
    void testPutCourseType_WhenCourseTypeDoesNotExist() {
        // Arrange
        Long id = 1L;
        CourseTypeDto inputDto = getCourseTypeDtos().get(0);

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CourseTypeNotFoundException.class, () -> courseTypeService.putCourseType(inputDto, id));
        verify(courseTypeRepository).findById(id);
    }
    @Test
    void testDeleteCourseType_WhenCourseTypeExists() {
        // Arrange
        Long id = 1L;
        CourseType courseType = getCourseTypes().get(0);

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.of(courseType));

        // Act
        ResponseEntity<?> response = courseTypeService.deleteCourseType(id);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(courseTypeRepository, times(1)).delete(courseType);
    }

    @Test
    void testDeleteCourseType_WhenCourseTypeDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(courseTypeRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CourseTypeNotFoundException.class, () -> courseTypeService.deleteCourseType(id));
    }
    private List<CourseTypeDto> getCourseTypeDtos() {
        CourseTypeDto courseTypeDto1 = new CourseTypeDto(1l, "type 1");
        CourseTypeDto courseTypeDto2 = new CourseTypeDto(2l, "type 2");
        return List.of(courseTypeDto1, courseTypeDto2);
    }
    private List<CourseType> getCourseTypes() {
        CourseType courseType1 = new CourseType("type 1");
        CourseType courseType2 = new CourseType("type 2");
        return List.of(courseType1, courseType2);
    }
}