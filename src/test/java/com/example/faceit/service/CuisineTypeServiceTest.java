package com.example.faceit.service;

import com.example.faceit.dtos.CuisineTypeDto;
import com.example.faceit.exception.CuisineTypeNotFoundException;
import com.example.faceit.mappers.Mapper;
import com.example.faceit.model.CuisineType;
import com.example.faceit.repository.CuisineTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CuisineTypeServiceTest {
    @Mock
    private CuisineTypeRepository cuisineRepository;
    @Mock
    private Mapper mapper;
    @InjectMocks
    private CuisineTypeService cuisineTypeService;

    @Test
    void testFindAll() {
        // Arrange
        List<CuisineType> cuisineTypes = getCuisineTypes();

        when(cuisineRepository.findAll()).thenReturn(cuisineTypes);
        List<CuisineTypeDto> expectedDtos = getCuisineTypeDtos();

        when(mapper.toDto(cuisineTypes.get(0))).thenReturn(getCuisineTypeDtos().get(0));
        when(mapper.toDto(cuisineTypes.get(1))).thenReturn(getCuisineTypeDtos().get(1));

        // Act
        List<CuisineTypeDto> result = cuisineTypeService.findAll();

        // Assert
        assertEquals(expectedDtos.size(), result.size());
        for (int i = 0; i < expectedDtos.size(); i++) {
            assertEquals(expectedDtos.get(i), result.get(i));
        }
    }

    @Test
    void testFindById_WhenCuisineTypeExists() {
        // Arrange
        Long id = 1L;
        CuisineType cuisineType = getCuisineTypes().get(0);
        CuisineTypeDto expectedDto = getCuisineTypeDtos().get(0);

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.of(cuisineType));
        when(mapper.toDto(cuisineType)).thenReturn(expectedDto);

        // Act
        CuisineTypeDto result = cuisineTypeService.findById(id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
    }

    @Test
    void testFindById_WhenCuisineTypeDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CuisineTypeNotFoundException.class, () -> cuisineTypeService.findById(id));
        verify(cuisineRepository).findById(id);
    }

    @Test
    void testCreateCuisineType() {
        // Arrange
        CuisineTypeDto inputDto = getCuisineTypeDtos().get(0);
        CuisineType entity = getCuisineTypes().get(0);
        CuisineType savedEntity = getCuisineTypes().get(1);
        CuisineTypeDto expectedDto = getCuisineTypeDtos().get(1);

        when(mapper.toEntity(inputDto)).thenReturn(entity);
        when(cuisineRepository.save(entity)).thenReturn(savedEntity);
        when(mapper.toDto(savedEntity)).thenReturn(expectedDto);

        // Act
        CuisineTypeDto result = cuisineTypeService.createCuisineType(inputDto);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
        verify(cuisineRepository).save(entity);
    }

    @Test
    void testPutCuisineType_WhenCuisineTypeExists() {
        // Arrange
        Long id = 2L;
        CuisineTypeDto inputDto = getCuisineTypeDtos().get(0);
        CuisineType cuisineType = getCuisineTypes().get(0);
        CuisineType savedCuisineType = getCuisineTypes().get(1);
        CuisineTypeDto expectedDto = getCuisineTypeDtos().get(1);

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.of(cuisineType));
        when(mapper.toEntity(inputDto)).thenReturn(cuisineType);
        when(cuisineRepository.save(cuisineType)).thenReturn(savedCuisineType);
        when(mapper.toDto(savedCuisineType)).thenReturn(expectedDto);

        // Act
        CuisineTypeDto result = cuisineTypeService.putCuisineType(inputDto, id);

        // Assert
        assertNotNull(result);
        assertEquals(expectedDto, result);
        assertEquals(id, result.getId());
    }

    @Test
    void testPutCuisineType_WhenCuisineTypeDoesNotExist() {
        // Arrange
        Long id = 1L;
        CuisineTypeDto inputDto = getCuisineTypeDtos().get(0);

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CuisineTypeNotFoundException.class, () -> cuisineTypeService.putCuisineType(inputDto, id));
    }
    @Test
    void testDeleteCuisineType_WhenCuisineTypeExists() {
        // Arrange
        Long id = 1L;
        CuisineType cuisineType = new CuisineType();

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.of(cuisineType));

        // Act
        ResponseEntity<?> response = cuisineTypeService.deleteCuisineType(id);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(cuisineRepository, times(1)).delete(cuisineType);
    }

    @Test
    void testDeleteCuisineType_WhenCuisineTypeDoesNotExist() {
        // Arrange
        Long id = 1L;

        when(cuisineRepository.findById(id)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        assertThrows(CuisineTypeNotFoundException.class, () -> cuisineTypeService.deleteCuisineType(id));
    }

    private List<CuisineTypeDto> getCuisineTypeDtos() {
        CuisineTypeDto cuisineType1 = new CuisineTypeDto(1L, "Cuisine Type1");
        CuisineTypeDto cuisineType2 = new CuisineTypeDto(2L, "Cuisine Type2");
        return List.of(cuisineType1, cuisineType2);
    }

    private List<CuisineType> getCuisineTypes() {
        CuisineType cuisineType1 = new CuisineType("Cuisine Type1");
        CuisineType cuisineType2 = new CuisineType("Cuisine Type2");
        return Arrays.asList(cuisineType1, cuisineType2);
    }
}